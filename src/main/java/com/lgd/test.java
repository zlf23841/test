package com.lgd;

import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;
import com.sun.net.httpserver.HttpServer;

import java.io.IOException;
import java.io.OutputStream;
import java.net.InetSocketAddress;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.Executors;

public class test {
    public static void main(String[] args) {
        InetSocketAddress addr = new InetSocketAddress(80);
        HttpServer server = null;
        try {
            server = HttpServer.create(addr, 0);
        } catch (IOException e) {
            e.printStackTrace();
        }

        server.createContext("/add", new HttpHandler() {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException {
                OutputStream response = init(httpExchange);
                response.write(add(httpExchange, getParams(httpExchange)).toString().getBytes(StandardCharsets.UTF_8));
                response.close();
            }
        });
        server.createContext("/mult", new HttpHandler() {
            @Override
            public void handle(HttpExchange httpExchange) throws IOException {
                OutputStream response = init(httpExchange);
                response.write(mult(httpExchange, getParams(httpExchange)).toString().getBytes(StandardCharsets.UTF_8));
                response.close();
            }
        });
        server.setExecutor(Executors.newCachedThreadPool());
        server.start();
    }

    public static OutputStream init(HttpExchange httpExchange) throws IOException {
        Headers responseHeaders = httpExchange.getResponseHeaders();
        responseHeaders.set("Content-Type", "application/json; charset=utf-8");
        httpExchange.sendResponseHeaders(200, 0);
        return httpExchange.getResponseBody();
    }
    public static List<String> getParams(HttpExchange httpExchange) throws IOException {
        String requestMethod = httpExchange.getRequestMethod();
        if (requestMethod.equalsIgnoreCase("GET")) {
            List<String> params = new ArrayList<>();
            for (String param : httpExchange.getRequestURI().getQuery().split("&")) {
                params.add(param.split("=")[1]);
            }
            return params;
        }
        return null;
    }

    private static Double add(HttpExchange httpExchange, List<String> number) throws IOException {
        Double result = null;
        if (number != null && number.size() >= 2) {
            result = 0.0;
            for (String num : number) {
                result += Double.valueOf(num);
            }
        }
        return result == null ? 0 : result;
    }

    private static Double mult(HttpExchange httpExchange, List<String> number) throws IOException {
        Double result = null;
        if (number != null && number.size() >= 2) {
            result = 1.0;
            for (String num : number) {
                result *= Double.valueOf(num);
            }
        }
        return result == null ? 0 : result;
    }
}
